# Subquery

## Konsep dan Penggunaan
- Subquery merupakan query DQL (SELECT) yang menjadi bagian dari query lainnya, DQL ataupun DML
- Dapat berada di dalam perintah SELECT, INSERT, UPDATE, DELETE, ataupun SET

## Referensi
- [Working with PostgreSQL Subquery | A 101 Guide - Hevo Data](https://hevodata.com/learn/postgresql-subquery/)
- [PostgreSQL - Sub Queries - Tutorialspoint](https://www.tutorialspoint.com/postgresql/postgresql_sub_queries.htm)
- [PostgreSQL Subquery - w3resource](https://www.w3resource.com/PostgreSQL/postgresql-subqueries.php)
- [PostgreSQL Subquery - PostgreSQLTutorial](https://www.postgresqltutorial.com/postgresql-tutorial/postgresql-subquery/)
