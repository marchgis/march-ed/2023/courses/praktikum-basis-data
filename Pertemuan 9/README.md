# Built-in Function
Merujuk kepada berbagai fungsi yang telah disediakan secara default dari vendor database.

No | Perintah | Deskripsi | Contoh penggunaan
---|---|---|---
1 | CONCAT | Menggabungkan string | 

## Perintah-Perintah yang Sering Digunakan

### CONCAT
Menggabungkan string
```sql
SELECT CONCAT('Satu', ' ', 'Satu', ' ', 'Aku', ' ', 'Sayang', ' ', 'Ibu')
```

### CAST
Mengkonversikan tipe database
```sql
SELECT CAST('000128' AS INTEGER) AS nomor
```

### SUBSTRING
Mengambil beberapa bagian dari string
```sql
-- Cara penulisan
SUBSTRING(<ISI_STRINGNYA>, <URUTAN_KARAKTER_MULAI_DIAMBIL>, <BERAPA_KARAKTER_YANG_DIAMBIL>)
```

```sql
SELECT 
	CASE
    	WHEN SUBSTRING('1187050098',1,1) = '1' THEN 'S1'
        WHEN SUBSTRING('1187050098',1,1) = '2' THEN 'S2'
        WHEN SUBSTRING('1187050098',1,1) = '3' THEN 'S3'
        ELSE '-' END
    as angkatan,
	SUBSTRING('1187050098',2,2) as angkatan,
    SUBSTRING('1187050098',4,3) AS kode_jurusan,
    CAST (SUBSTRING('1187050098',7,4) AS INTEGER) as nomor_absen
```

### LOWER dan UPPER
Membuat string jadi huruf kecil semua atau huruf besar semua
```sql
SELECT UPPER('chat nya biasa aja jangan pake capslock !')
```
```sql
SELECT LOWER('nama jangan ditulis huruf kecil semua !')
```

### LPAD dan RPAD
Menambahkan sejumlah karakter dikanan atau dikiri string sesuai maksimum jumlah karakter yang ditentukan. Contohnya 1 diubah menjadi 0001
```sql
SELECT LPAD('19',4,'0')
```

## Referensi
- [Numeric function](https://www.tutorialspoint.com/postgresql/postgresql_numeric_functions.htm)
- [String function](https://www.tutorialspoint.com/postgresql/postgresql_string_functions.htm)
- [Array function](https://www.tutorialspoint.com/postgresql/postgresql_array_functions.htm)
- [JSON function](https://www.postgresqltutorial.com/postgresql-tutorial/postgresql-json/)
