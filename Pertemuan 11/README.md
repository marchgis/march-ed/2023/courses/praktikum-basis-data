# Transaction

## Konsep dan Penggunaan
- Satu unit kerja
- Berisi satu atau lebih query SQL
- Tiap query SQL harus berhasil, jika salah satu gagal, maka semua gagal

## Referensi
- [Transactions - PostgreSQL.org](https://www.postgresql.org/docs/current/tutorial-transactions.html)
- [Transactions - PostgreSQLTutorial.com](https://www.postgresqltutorial.com/postgresql-tutorial/postgresql-transaction/)
- [Transactions - TutorialsPoint.com](https://www.tutorialspoint.com/postgresql/postgresql_transactions.htm)
- [Transactions - enterprisedb.com](https://www.enterprisedb.com/postgres-tutorials/how-work-postgresql-transactions)
