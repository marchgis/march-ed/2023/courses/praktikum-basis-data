# Procedure, Function, dan Trigger

## Referensi

### PostgreSQL Stored Procedure & Function
- [PostgreSQL functions - Javatpoint](https://www.javatpoint.com/postgresql-functions)
- [Everything you need to know about Postgres stored procedures and functions](https://www.enterprisedb.com/postgres-tutorials/everything-you-need-know-about-postgres-stored-procedures-and-functions)
- [Query Language (SQL) Functions - PostgreSQL.org](https://www.postgresql.org/docs/current/xfunc-sql.html)
- [PostgreSQL CREATE PROCEDURE - PostgreSQLTutorial](https://www.postgresqltutorial.com/postgresql-plpgsql/postgresql-create-procedure/)
- [PostgreSQL Stored Procedures: The Ultimate Guide with Examples - Tutorialspoint](https://hevodata.com/learn/postgresql-stored-procedures/)

### PostgreSQL Trigger
- [Create Trigger - PostgreSQL.org](https://www.postgresql.org/docs/current/sql-createtrigger.html)
- [PostgreSQL Triggers - PostgreSQLTutorial.com](https://www.postgresqltutorial.com/postgresql-triggers/)
- [PostgreSQL - TRIGGERS - Tutorialspoint.com](https://www.tutorialspoint.com/postgresql/postgresql_triggers.htm)
- [Everything you need to know about PostgreSQL triggers - EDB](https://www.enterprisedb.com/postgres-tutorials/everything-you-need-know-about-postgresql-triggers)
