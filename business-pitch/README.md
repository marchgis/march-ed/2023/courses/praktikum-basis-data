# Business Pitch

## Kriteria Penilaian
1. Menampilkan use case table untuk produk digitalnya
2. Mampu mendemonstrasikan web service (CRUD) dari produk digitalnya:
    - Screenshot koneksi ke database dari bahasa pemrograman yang dipilih
    - Bentuk web servicenya:
        - RESTful API
        - GraphQL
    - Interface untuk eksplorasi web servicenya (Hoppscotch / Postman / Swagger)
3. Mampu mendemonstrasikan minimal 3 visualisasi data untuk business intelligence produk digitalnya:
    - Pilih:
        - Via aplikasi client seperti Javascript / Flutter / dsb.
        - via business intelligence tool seperti Google Looker Studio / Google Colab / Apache Superset / Tableau / dsb.
4. Mampu mendemonstrasikan penggunaan minimal 3 built-in function dengan ketentuan
    - 1 Regex
    - 1 Substring
    - Sisanya bebas
5. Mampu mendemonstrasikan dan menjelaskan penggunaan Subquery pada produk digitalnya
6. Mampu mendemonstrasikan dan menjelaskan penggunaan Transaction pada produk digitalnya
7. Mampu mendemonstrasikan dan menjelaskan penggunaan Procedure / Function dan Trigger pada produk digitalnya
8. Mampu mendemonstrasikan Data Control Language (DCL) pada produk digitalnya
9. Mampu mendemonstrasikan dan menjelaskan constraint yang digunakan pada produk digitalnya:
    - Minimal 1 Foreign Key
    - Minimal 1 Index
    - Minimal 1 Unique Key
10. Mendemonstrsikan produk digitalnya kepada publik dengan cara-cara kreatif melalui video Youtube
11. BONUS !!! Mendemonstrasikan UI untuk CRUDnya
